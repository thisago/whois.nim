<!--
  :Author: Thiago Navarro
  :Email: thiago@oxyoy.com

  **Created at:** 06/09/2021 22:46:52 Wednesday
  **Modified at:** 06/09/2021 10:48:08 PM Wednesday

  ------------------------------------------------------------------------------

  changelog
  ------------------------------------------------------------------------------
-->

# Changelog

## Version 1.0.1

- Fixed a bug that increments the name servers and not deletes the old ones on update
